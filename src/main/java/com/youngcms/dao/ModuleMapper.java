package com.youngcms.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.youngcms.bean.Module;

public interface ModuleMapper extends BaseMapper<Module> {
}