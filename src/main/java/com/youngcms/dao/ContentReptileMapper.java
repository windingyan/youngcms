package com.youngcms.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.youngcms.bean.ContentReptile;

public interface ContentReptileMapper extends BaseMapper<ContentReptile> {
}