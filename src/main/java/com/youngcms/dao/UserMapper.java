package com.youngcms.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.youngcms.bean.User;

public interface UserMapper extends BaseMapper<User> {
}