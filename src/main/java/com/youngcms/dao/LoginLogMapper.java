package com.youngcms.dao;

import com.youngcms.bean.LoginLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
  *  Mapper 接口
 * </p>
 *
 * @author fumiao
 * @since 2017-08-07
 */
public interface LoginLogMapper extends BaseMapper<LoginLog> {

}