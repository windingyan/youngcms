package com.youngcms.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.youngcms.bean.CmsModel;

public interface CmsModelMapper extends BaseMapper<CmsModel> {
}