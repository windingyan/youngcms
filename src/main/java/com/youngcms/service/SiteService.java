package com.youngcms.service;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.youngcms.bean.Site;
import com.youngcms.dao.SiteMapper;
@Service
public class SiteService extends ServiceImpl<SiteMapper,Site> {
}
