package com.youngcms.service;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.youngcms.bean.Advert;
import com.youngcms.dao.AdvertMapper;

@Service
public class AdvertService extends ServiceImpl<AdvertMapper, Advert> {
}
